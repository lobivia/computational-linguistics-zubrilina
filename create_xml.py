#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import codecs
import re
from lxml import etree

def texts_to_xml(directory):
    files = os.listdir(directory)
    page = etree.Element('Files')

    for file in files:
        headElement = etree.SubElement(page, 'File')
        with codecs.open((directory + '/' + file), 'r', encoding='utf-8') as f:
            text_in = f.read()
            f.close()
        for sentence in re.split(r"[\.!?\n]", text_in):
            title = etree.SubElement(headElement, 'Sentence')
            title.text = re.sub(r"[«»\"–()\.,%/—\-:;\n]", '', sentence)
            with codecs.open(directory + '.xml', 'w', "utf-8") as output:
                output.write(etree.tounicode(page, pretty_print = True))

directory1 = 'politics'
directory2 = 'sport'

texts_to_xml(directory1)
texts_to_xml(directory2)

