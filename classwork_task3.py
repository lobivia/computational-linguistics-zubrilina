import nltk
from  scipy import spatial
from task2 import get_sentences, normalize_words, create_index

def get_top5_min_cos_dist(cos_dist):
    return sorted(cos_dist, key=cos_dist.get, reverse=True)[:5]

file = 'sport.xml'
sentences=get_sentences(file)
inverted_index = create_index(sentences)

while True:
    query = input("Please, enter search query\n")
    search_words = normalize_words(nltk.word_tokenize(query))

    result = {} # set of texts contain at least one word from query
    for word in search_words:
        found_words = inverted_index.get(word)
        if found_words:
            if len(result) == 0:
                result = set(found_words)
            else:
                result = result.union(found_words)
    #print("\nThis words in sentences: ", result)

    texts_bin_vectors = {}
    for text in result:
        text_bin_vec = []
        for word in search_words:
            if text in inverted_index.get(word):
                text_bin_vec.append(1)
            else:
                text_bin_vec.append(0)
        texts_bin_vectors.update({text: text_bin_vec})

    query_bin_vector = []
    for word in search_words:
        if inverted_index.get(word):
            query_bin_vector.append(1)
        else:
            query_bin_vector.append(0)

    cos_dist={}
    for text, text_bin_vec in texts_bin_vectors.items():
        dist = spatial.distance.cosine(text_bin_vec, query_bin_vector)
        cos_dist.update({text: dist})

    print(get_top5_min_cos_dist(cos_dist))
