import nltk
import collections
import math
from  scipy import spatial
from task2 import get_sentences, normalize_words, create_index
from classwork_task3 import get_top5_min_cos_dist

def get_norm_sentences(sentences):
    norm_sentences =[]
    for sent in sentences:
        tokens =nltk.word_tokenize(sent)
        words = normalize_words(tokens)
        words = set(words)
        norm_sentences.append(words)
    return norm_sentences

def creat_index_tfidf(sentences):
    tfidf_index = {}
    text_id =0;
    texts_tfidf = compute_td_idf(sentences) # type - list of dictionaries
    for text in texts_tfidf:
        for word, tfidf in text.items():
            if tfidf_index.get(word):
                tfidf_index.get(word).update({text_id: tfidf})
            else:
                tfidf_index.update({word: {text_id: tfidf}})
        text_id += 1
    return tfidf_index

def compute_tf(text):
    tf = collections.Counter(text) #tokens frequency
    for i in tf:
        tf[i] = tf[i]/float(len(text))
    return tf

def compute_idf(word, sentences):
    return math.log10(len(sentences) / sum([1.0 for i in sentences if word in i]))

def compute_td_idf(sentences):
    texts_list = []
    for text in sentences:
        tf_idf = {} #dictionary to store tf_idf for each word
        tf = compute_tf(text)
        for word in tf :
            tf_idf[word] = tf[word]*compute_idf(word, sentences)
        texts_list.append(tf_idf)
    return texts_list

file = 'sport.xml'
sentences = get_norm_sentences(get_sentences(file))
tfidf_index = creat_index_tfidf(sentences)

while True:
    query = input("Please, enter search query\n")
    search_words = normalize_words(nltk.word_tokenize(query))

    result = set() # set of texts contain at least one word from query
    for word in search_words:
        found_words = tfidf_index.get(word)
        if found_words:
            if len(result) == 0:
                result = set(tfidf_index.get(word).keys())
            else:
                result = result.union(tfidf_index.get(word).keys())
        #print (result)

    texts_tf_vectors = {}
    for text_id in result:
        text_tf_vec = []
        for word in search_words:           
            if tfidf_index.get(word) and text_id in tfidf_index.get(word).keys():
                text_tf_vec.append(tfidf_index.get(word).get(text_id))
            else:
                text_tf_vec.append(0)
        texts_tf_vectors.update({text_id: text_tf_vec})

    query_tf_vector = []
    query_tf = compute_tf(search_words)
    for word in search_words:
        query_tf_vector.append(query_tf[word])

    cos_dist={}
    for text, text_bin_vec in texts_tf_vectors.items():
        dist = spatial.distance.cosine(text_bin_vec, query_tf_vector)
        cos_dist.update({text: dist})

    print(get_top5_min_cos_dist(cos_dist))