import os
import codecs
import re
from lxml import etree
import nltk
from nltk.corpus import stopwords
import pymorphy2

def get_sentences(file):
    tree = etree.parse(file)
    nodes = tree.xpath('/Files/File/Sentence')
    sentences = []
    for node in nodes:
        sentences.append(node.text)
    return sentences

def normalize_words(words):
    morph = pymorphy2.MorphAnalyzer()
    stop_words = stopwords.words('russian')
    words_filtered = [i for i in words if (i not in stop_words)]
    norm_words = []
    for word in words_filtered:
        norm_words.append(morph.parse(word)[0].normal_form)
    return norm_words

def create_index(sentences):
    inverted_index = {}
    sent_number = 0;
    for sent in sentences:
        tokens = nltk.word_tokenize(sent)
        words = normalize_words(tokens)
        words = set(words)
        for word in words:
            if inverted_index.get(word):
                inverted_index.get(word).append(sent_number)
            else :
                inverted_index.update({word: [sent_number]})
        sent_number += 1
    print("Index has built")
    return inverted_index


file = 'sport.xml'
sentences = get_sentences(file)
inverted_index = create_index(sentences)
#print(inverted_index.keys())
while True:
    query = input("Please, enter search query\n")
    words = normalize_words(nltk.word_tokenize(query))
    result = {}
    for word in words:
        found_words = inverted_index.get(word)
        if found_words:
            if len(result) == 0:
                result = set(found_words)
            else:
                result = result.intersection(found_words)
    print("\nThis words in sentences: ", result)
    for i in result:
        print (sentences[i])